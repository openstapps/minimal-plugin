/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {HttpClient} from '@openstapps/api/lib/http-client';
import {PluginClient} from '@openstapps/api/lib/plugin-client';
import {Converter} from '@openstapps/core-tools/lib/schema';
import {Logger} from '@openstapps/logger';
import {Command, Option} from 'commander';
import {readFileSync} from 'fs';
import {join, resolve} from 'path';
import {MinimalPlugin} from './plugin/minimal-plugin';


process.on('unhandledRejection', (error) => {
  throw error;
});

const pluginVersion = JSON.parse(
  readFileSync(join(__dirname, '..', 'package.json'))
    .toString(),
).version;

const program = new Command()
  .version(pluginVersion)
  // tslint:disable: newline-per-chained-call
  .addOption(new Option('-b, --backend-url <string>', 'URL of the StApps backend deployment').default('http://localhost:3000').env('STAPPS_BACKEND'))
  .addOption(new Option('-n, --plugin-name <string>', 'The name of the plugin').default('minimal-plugin')) // TODO: adjust default
  .addOption(new Option('-r, --route-name <string>', 'The name of the route').default('minimalplugin')) // TODO: adjust default
  .addOption(new Option('-u, --url <string>', 'The url of the plugin').default('http://localhost').env('PLUGIN_URL')) // TODO: adjust default
  .addOption(new Option('-p, --port <number>', 'The port of the plugin').default('4000').env('PLUGIN_PORT')) // TODO: adjust default
  .parse(process.argv);
  // tslint:enable: newline-per-chained-call

const options = program.opts();

// create an instance of the PluginClient
const pluginClient = new PluginClient(new HttpClient(), options.backendUrl);

// create an instance of your plugin
const plugin = new MinimalPlugin(
  // tslint:disable-next-line:no-magic-numbers
  Number.parseInt(options.port, 10), options.pluginName, options.url, `/${options.routeName}`, options.backendUrl,
  new Converter(resolve(__dirname, '..', 'src', 'plugin', 'protocol')), // an instance of the converter. Required
  // because your requests and response schemas are defined in the plugin. The path should lead to your request and
  // response interfaces
  'SCMinimalRequest', // TODO: adjust name of the request interface
  'SCMinimalResponse', // TODO: adjust name of the response interface
  JSON.parse(readFileSync(resolve(__dirname, '..', 'package.json'))
    .toString()).version, // get the version of the plugin from the package.json
);

pluginClient.registerPlugin(plugin)
  .then(() => {
    Logger.ok(`Successfully registered plugin '${options.pluginName}' on /${options.routeName} .`);
  })
  .catch((err: Error) => {
    throw err;
  });

[`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `SIGTERM`].forEach((signal) => {
  process.once(signal as NodeJS.Signals, () => {
    pluginClient.unregisterPlugin(plugin)
    .then(() => {
      Logger.ok(`Successfully unregistered plugin '${options.pluginName}' from /${options.routeName} .`);
    })
    .catch((err: Error) => {
      throw err;
    });
  }) ;
});
