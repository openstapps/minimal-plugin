/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * The Request Interface
 *
 * All your responses to the backend are required to look like this. You need to add the @validatable tag like shown
 * below for the plugin to work. The response can have any layout you like.
 * TODO: remove body of the interface and replace with your own layout
 *
 * @validatable
 */
export interface SCMinimalResponse {
  /**
   * The sum of the numbers from the request
   */
  sum: number;
}
