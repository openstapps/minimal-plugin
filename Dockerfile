FROM registry.gitlab.com/openstapps/projectmanagement/node

WORKDIR /app
ENTRYPOINT ["node", "lib/cli.js"]

ADD . /app
