# minimal-connector

## Prerequisites

* `node` and `npm` installed
* a backend, which is running locally (e.g. on http://localhost:3000)

## How to get started

To install all required `npm` packages:

```shell script
npm install
```

To prepare the script to be executed, run:

```shell script
npm run build
```

To start the plugin, you need to execute the code in "CLI" script:

```shell script
node lib/cli.js -b <backendURL> -n <pluginName> -r <routeName> -u <URL> -p <port>

With fallback values being:
backendURL = http://localhost:3000 , pluginName = minimal-plugin , 
routeName = minimalplugin , URL = http://localhost, port = 4000
```

Using environment variables you can set backendURL via `STAPPS_BACKEND`, URL via `PLUGIN_URL` and port via `PLUGIN_URL`.

## Creating your own plugin

* Modify the default values in `cli.ts` to your needs:
  * update the name
  * update the port
  * update the route
  * update your request and response names (You need these in a bit)
* Rename the `minimal-plugin.ts` and the `MinimalPlugin` class to a more suitable name and adjust the imports in other files
* Delete the `sum` function in `minimal-plugin.ts` and write your own logic inside the `onRouteInvoke` function
* Define the layout of your http requests and responses inside the `/plugin/protocol` folder. The names of the interfaces
need to be the same as the request and response names you defined in the `cli.ts`

## Code structure

Folder `src` contains:

* Reference implementations for CLI and a plugin
* [/cli.ts](src/cli.ts)
  * minimal CLI to start your connector, that uses CLI-args, so that there are no hard coded values for configuration
  * will execute the specified connectors and push the data to the backend
* [/common.ts](src/common.ts) blank file for commonly used values across files to put in
* [/plugin/minimal-connector.ts](src/plugin/minimal-plugin.ts) minimal plugin class with some example logic
* [/plugin/protocol/request.ts](src/plugin/protocol/request.ts) the interface for any incoming http requests
* [/plugin/protocol/response.ts](src/plugin/protocol/response.ts) the interface for responses by the plugin to the backend
* [/plugin/protocol/tsconfig.json](src/plugin/protocol/response.ts) required by schema generator
